import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import itertools
from collections import OrderedDict
from datetime import datetime
import glob
import re
import ROOT as rt


def read_from_file(fnames):
      core=[]
      #core={}
      #dicto={}
      for fname in fnames:
            with open(fname, 'r') as f:
                  fstring = f.readlines()
                  
            date=''
            adc=''
            rog=''
            qty=''
            dicto={}
            for line in fstring:
                  measurement = line.split()
                  measurement = [x.replace(",","") for x in measurement]
                  measurement =[x.replace("'","") for x in measurement]

                  #core.append( measurement)
                  #dicto={}
                  #if len(measurement)==15 and ('vd' in measurement[5] or 'va' in measurement[5]) :
                        #qty=measurement[5]
                        #continue
                  if len(measurement)==15 and ('ROC' in measurement[3]):
                        rog=measurement[3]
                        adc=measurement[9]
                        date="".join(measurement[11]+measurement[12])
                        qty=measurement[5]
                       # dicto.setdefault(measurement[3],{}).update({'qty':qty,'adc':adc,'tstamp':date})
                        #dicto.setdefault(measurement[3],{}).update({'adc':adc})
                        dicto[rog]=adc
                        #dicto.update({'qty':qty,'tstamp':date,'adc':adc,'rog':rog})
                        #print dicto
                        #print rog
                        #print adc
                        #print date
                  else:
                        continue
                        #print dicto
            core.append(dicto)
            
      return core








#def read_from_file_Readback(filenames):
def Read_Readback_sample(fnames):


          Parent=[]

          for fname in fnames:
                    with open(fname, 'r') as f:
                     fstring = f.readlines()
                     filepath=f.name
                     #print filepath                                                                                                                                                                                                                                           
                     #date=timestamp(filepath)                                                                                                                                                                                                                                 
                     #print date                                                                                                                                                                                                                                               
                     #print date                                                                                                                                                                                                                                               
                     name=''
                     dictoname={}
                    for line in fstring:
                              measurement = line.split()
                              if len(measurement)== 3 and ('FPix_' in measurement[1] or 'BPix_' in measurement[1]):
                                        name=measurement[1]
                                        dictoname.setdefault(measurement[2],{}).update({'Name':name})#initialize a dictionary with the increasing integer as the key and the name and TImestamp as values added later                                                          
                              if len(measurement)== 6 and not "#" in measurement[0]:
                                        dictoname.setdefault(measurement[0],{}).update({'vd':measurement[1],'va':measurement[2],'vana':measurement[3],'vbg':measurement[4],'iana':measurement[5]} )


                              else:
                                  continue
                    Parent.append(dictoname)
          return Parent

def dict_from_dict(Finals):
          #Finals =Read_Readback_sample(fnames)                                                                                                                                                                                                                                
          name=[]
          va_adc=[]
          vd_adc=[]
          rog_dict={}
          for Final in Finals:
                    for key in Final.keys():
                              if  "BPix" in Final[key]["Name"]:
                                        rog=Final[key]["Name"]
                                        va=Final[key]['va']
                                        vd=Final[key]['vd']
                                        va_adc.append(va)
                                        vd_adc.append(vd)
                                        name.append(rog)
                              if name[-1] not in rog_dict :
                                    #rog_dict[name[-1]]=[]
                                    rog_dict[name[-1]]=va_adc[-1]
                                    #rog_dict[name[-1]]=vd_adc[-1]
                                        #rog_dict[name[-1]]=[[],[]]
                                        #rog_dict[name[-1]][0].append(vd_adc[-1])
                                        #rog_dict[name[-1]][1].append(va_adc[-1])
                                    

          return rog_dict






if __name__ == "__main__":

      c1 = rt.TCanvas('c1','V_a Differece Histogram')
      #pad1=pad1 =rt.TPad( 'pad1', 'The pad with the function',  0.05, 0.50, 0.95, 0.95, 21 )
      #pad1=rt.TPad('pad1','The pad with difference',0.05, 0.50, 0.95, 0.95, 21 )
      #pad1.Draw()
      #pad1.cd()
      #histodiff=rt.TH1F('histodiff','Spread in Differences in va for VaBPix_va - Readback_va',10,-5,5)
      histodiff=rt.TH1F('histodiff','Spread in Differences in vd for VdigBPix_vd_Run2686 - VdigBPix_vd_Run2647',10,-5,5)

      histodiff.SetFillColor( 45 )
      histodiff.GetXaxis().SetTitle("Differences");
      histodiff.GetYaxis().SetTitle("Frequency");
      #return core
      fnames1=["vd2.txt"]
      #fnames1=["vd.txt"]
      dict1=read_from_file(fnames1)[0]
      #print dict1
      fnames2=["Vd1.txt"]
      #dict_=(fnames2)
      dict2=read_from_file(fnames2)[0]
      #print dict2
      #dict3 = dict([(key, abs(float(dict1[key]) - float(dict2[key]))) for key in dict1 if key in dict2])
      dict3 = dict([(key,(float(dict1[key]) - float(dict2[key]))) for key in dict1 if key in dict2]) 
      #dict3 = {key:dict2[key] for key in dict1 if key in dict2}
     # key='BPix_BmI_SEC8_LYR4_LDR29F_MOD3_ROC7'
      #key2='BPix_BmI_SEC8_LYR4_LDR29F_MOD3_ROC3'
      #print dict1[key]                        
      #print dict2[key]
      #print dict3[key]
     # print dict3 
      name=[]
      va_diff=[]
      #vd_diff=[]
      roc_dict=OrderedDict()
      for key in dict3.keys():
            rog=key
            va_adc_diff=dict3[key]
            #vd_adc_diff=dict3[key]
            name.append(rog)
            va_diff.append(va_adc_diff)
            #vd_diff.append(vd_adc_diff)
            if name[-1] not in roc_dict:
                  roc_dict[name[-1]]=[[]]
                  #roc_dict[name[-1]][0].append(vd_diff[-1])
                  roc_dict[name[-1]][0].append(va_diff[-1])


      #print roc_dict
      roc_names = [] 
      y_a=[]
      #y_d=[]
      
      for roc in sorted(roc_dict):                                                                                                                                                                                                                                           
            if "BPix_BmI_SEC8_LYR1".lower() in roc.lower(): 
            #if "BPix_BmI_SEC8_LYR4_LDR31F".lower() in roc.lower():
           # if "BPix_BmI_SEC8_LYR4_LDR29F".lower() in roc.lower():
                  
                   #y_a.append(rog_dict[rog][0])                                                                                                                                                                                                                              
                   #lbl_a.append(rog_dict[rog][1])                                                                                                                                                                                                                            
                   #elif 'vd' in rog_dict[rog][1]:                                                                                                                                                                                                                            
                  y_a.append(roc_dict[roc][0])
                  #y_d.append(roc_dict[roc][0])

                   #lbl_d.append(rog_dict[rog][1])                                                                                                                                                                                                                            
                  roc_names.append(roc)
                  


      #print vd_diff
      print va_diff
      for i in va_diff:
            if i > 5.0:
                  histodiff.Fill(4.5)
            elif i < -5.0:
                  histodiff.Fill(-4.5)
            else:
                  histodiff.Fill(float(i))
      histodiff.Draw()
      c1.Update()
      c1.SaveAs("defd.png")
      #print type(y_a)
      
      x = range(len(roc_names))                                                                                                                                                                                                                                              
      #print len(y_a)
      #print len(x)
      plt.xticks(x,roc_names,rotation='vertical')
      plt.plot(x,y_a,marker=".",linestyle="-",label="vd")
      #histodiff.Fille(y_a)
                                                                                                                                                                                                                     
      plt.subplots_adjust(bottom=0.3)                                                                                                                                                                                                                                        
      plt.legend()
      
      #plt.show()                                                                                                                                                                                                                                                             
      #plt.Savefig("plot.png")



















      #print dict2
      #dict3 = dict([(key, abs(float(dict1[key]) - float(dict2[key]))) for key in dict1 if key in dict2])
      #dict3 = {key:dict2[key] for key in dict1 if key in dict2}
      #print dict3
#print rows
      #Adc=[]
      #Qty=[]
      #tstmp=[]
      # name=[]
      # #combo=[]
      # rog_dict=OrderedDict()
      # for row  in dict1:
      #       for key in row.keys():
      #             adc=row[key]['adc']
      #             #qty=row[key]['qty']
      #            # date=row[key]['tstamp']
      #             rog=key
      #             # else:
      #             Adc.append(adc)     # continue
      #             #Qty.append(qty)
      #             name.append(rog)
      # #print Qty
      #             if name[-1] not in rog_dict :
      #                   #rog_dict[name[[1]]=[[],[]]
      #                   rog_dict[name[-1]]=[[]]
      #                   rog_dict[name[-1]][0].append(Adc[-1])
      #                  # rog_dict[name[-1]][1].append(Qty[-1])
      # #print rog_dict
      #       #tstmp.append(date)
      # rog_names = []
     ## #y_a=[]
      # y_d=[]
      # #lbl_a=[]
      # lbl_d=[]
      # for rog in sorted(rog_dict):
      #       if "BPix_BmI_SEC8_LYR1".lower() in rog.lower():
      #             #if  'va' in rog_dict[rog][1]:
      #             #y_a.append(rog_dict[rog][0])
      #             #lbl_a.append(rog_dict[rog][1])
      #        #     elif 'vd' in rog_dict[rog][1]:
      #             y_d.append(rog_dict[rog][0])
      #             #lbl_d.append(rog_dict[rog][1]) 
      #           #  if not rog in rog_names:
      #             rog_names.append(rog)
                  
      # x = range(len(rog_names))
      # #print len(y_a)
      # print len(y_d)
      # print len(x)
      # plt.xticks(x,rog_names,rotation='vertical')
      # plt.plot(x,y_d,marker=".",linestyle="_",label="va")
      # #plt.plot(x,y_d,marker=".",linestyle="_",label="va")
      # plt.subplots_adjust(bottom=0.3)
      # plt.legend()
      # plt.show()
 
     #### for rog in sorted(rog_dict):
      #        x=rog
      #        y=rog_dict[rog][0]
      #        lbl=rog_dict[rog][1]
      #        #y_sort=[iy for _,iy in sorted(zip(x,y))]
      #        plt.plot(sorted(x),y,marker=".",linestyle="-",label=lbl)
      #        plt.legend()
      #        plt.savefig("test.png")

      #print combo
      #print Qty
      #print name
            #if name[-1] not in rog_dict:
                  


                  











    ### print tstmp
#rog_dict=OrderedDict()
      #keylist=rows.key()
      #keylist.sort()
      #for key in keylist:

           # if key=='va':
            #      va_adc=rows[key][1]
             #     date_va=rows[key][0]
              #    rog_va=rows[key][2]
           # elif key=='vd':
           #       vd_adc=rows[key][1]
           #       date_vd=rows[key][0]
           #       rog_vd=rows[key][2]
                 
           # else:
           #       continue
     # print va_adc
     # print date_va
     # print rog_va
