import cx_Oracle
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import itertools
from collections import OrderedDict 
con = cx_Oracle.connect("occupancyproblems_admin/pixDB2018@devdb12u")
cur = con.cursor()
   ## sql_insert_line="INSERT INTO DCUSNAPSHOTS (tstamp,rog,bottop,prt,anaone,digione,anatwo,digitwo,rtd,vbg,intrtd) VALUES (:tstamp,:rog,:bottop,:prt,:anaone,:digone,:anatwo,:digtwo,:rtd,:vbg,:intrtd)"                                                                     
sql_fetch_data="SELECT tstamp,rog,bottop,prt,rtd from dcusnapshots where tstamp !=TO_TIMESTAMP('27-APR-18 03.52.14.00000000 PM','DD-Mon-RR- HH:MI:SS.FF PM')"
# sql_fetch_data="SELECT tstamp,rog,bottop,prt,rtd from dcusnapshots"                                                                                                                                                                                                       
    #cur.prepare(sql_fetch_data)                                                                                                                                                                                                                                               
cur.execute(sql_fetch_data)
rows=cur.fetchall()
    #str(rows)[0:300]                                                                                                                                                                                                                                                          
    #dfrm= pd.DataFrame( [[ij for ij in i] for i in rows] )                                                                                                                                                                                                                    
    #dfrm.rename(columns={0: 'Timestamp', 1: 'Rog', 2: 'bot-top', 3: 'prt', 4:'RTD'}, inplace=True);                                                                                                                                                                           
combination={}
rog_dict=OrderedDict()
xvals=[]
yvals=[]
label=[]
for row in rows:
        xvals.append(row[0])
        yvals.append((float(row[3])/float(row[4])-1.0)*519.5)
        #point = (row[0],(float(row[3])/float(row[4])-1.0)*519.5)
        label.append(row[1]+row[2])
        if label[-1] not in rog_dict:
            rog_dict[label[-1]]=[[],[]]
            rog_dict[label[-1]][0].append(row[0])
            rog_dict[label[-1]][1].append(yvals[-1])
            
            #combination.setdefault(label,[]).append(point)
for (i,j,k) in itertools.izip(xvals,yvals,label):
    point=(i,j)
    combination.setdefault(k,[]).append(point)

for rog in sorted(rog_dict):
    if "BmI" in rog and "D1" in rog:
        x=rog_dict[rog][0]
y=rog_dict[rog][1]

y_sort=[iy for ix,iy in sorted(zip(x,y))]
plt.plot(sorted(x),y_sort,marker=".",linestyle="-",label=rog)
#input()
plt.legend()
plt.show()
plt.savefig("test.png")
