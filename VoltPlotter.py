import cx_Oracle
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import itertools
from collections import OrderedDict
con = cx_Oracle.connect("rhabibul/XYZ11xyz22@devdb12u")
cur = con.cursor()

sql_fetch_data="SELECT tstamp,rog,bottop,anaone,digione,anatwo,digitwo from dcusnapshots where tstamp !=TO_TIMESTAMP('27-APR-18 03.52.14.00000000 PM','DD-Mon-RR- HH:MI:SS.FF PM')"


cur.execute(sql_fetch_data)
rows=cur.fetchall()


rog_dict=OrderedDict()
xvals=[]
yvals=[]
zvals=[]
label=[]
#print rows
for row in rows:
    
    xvals.append(row[0])#Store the timestamp value here
    yvals.append(row[3]/float(1000))#store the Temp value here
    zvals.append(row[4]/float(1000))
    label.append(row[1]+row[2])#store the label value here
    if label[-1] not in rog_dict:#if the last stored value is not in rog_dict
        rog_dict[label[-1]]=[[],[],[]]#a dictionary with two empty lists with the label as the key
    rog_dict[label[-1]][0].append(row[0])# the first list will have the time stamp
    rog_dict[label[-1]][1].append(yvals[-1])# the second list will have the last voltage values
    rog_dict[label[-1]][2].append(zvals[-1])
Cylinders=['BpO','BpI','BmO','BmI']
Disks=['D1','D2','D3']
#for x in Cylinders:
   # print x
for rog in sorted(rog_dict):
    #for a in Cylinders:
        #for b in Disks:
    if 'BpO' in rog and  'D1' in rog:
                                                                                                                                                                                                                                                              
        x=rog_dict[rog][0]#variable to store the values of time stamp
        y=rog_dict[rog][1]#variable to store values of the Temp
        z=rog_dict[rog][2]
        y_sort=[iy for _,iy in sorted(zip(x,y))]#sorting calues in y using values from x
        z_sort=[iz for _,iz in sorted(zip(x,z))] 
        plt.plot(sorted(x),y_sort,marker=".",linestyle="-",label=rog+"AnalogVoltage-1")#plot
        plt.plot(sorted(x),z_sort,marker='*',linestyle="-",label=rog+"DigitalVoltage-1")
        plt.xlabel('timestamp')
        plt.ylabel('Voltage(Volt)')
        #input()                                                                                                                                                                                                                                                            
plt.legend(loc='lower right')
plt.show()
plt.savefig("test.png")
