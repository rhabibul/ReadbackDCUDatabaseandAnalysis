import pixdb
from datetime import datetime
## later exchange
with open('log.txt') as f:
    readback_dicts = eval(f.read())
#print readback_dicts

with pixdb.PixdbCoreInterface() as interface:

    sql_query_part_ids = "SELECT cms_pix_names.name,cms_pix_parts.part_id from cms_pix_names JOIN cms_pix_part_name ON cms_pix_names.name_id=cms_pix_part_name.name_id JOIN cms_pix_parts ON cms_pix_parts.part_id=cms_pix_part_name.part_id"
    sql_query_catalog_id = "SELECT cms_pix_measurement_types.measurement_type ,cms_pix_measurement_catalog.measurement_catalog_id FROM cms_pix_measurement_catalog JOIN cms_pix_measurement_types ON cms_pix_measurement_types.measurement_type_id = cms_pix_measurement_catalog.measurement_type_id JOIN cms_pix_part_types ON cms_pix_part_types.part_type_id=cms_pix_measurement_catalog.part_type_id WHERE part_type LIKE 'ROC'"
    part_ids=dict(interface.query(sql_query_part_ids))
    
    measurement_catalog_ids=dict(interface.query(sql_query_catalog_id))
    print measurement_catalog_ids
    for line in readback_dicts:
        for ikey,vals in line.iteritems():
            date= vals['Timestamp']
            iov_date=datetime.strptime(date,'%Y-%m-%d %H:%M:%S.%f')
            #print iov_date
            #print iov_date
            part_id=part_ids[vals['Name']]
            #print part_id
            for qty, val in vals.iteritems():
                if not qty in measurement_catalog_ids:
                    continue
                measurement_catalog_id=measurement_catalog_ids[qty]
                measurement = [(val,None)]
                interface.insert_measurement(part_id,measurement_catalog_id,measurement,iov_date)

                
