import ROOT as rt
import os
import datetime
import glob
import math
import sys
import pixdb

script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
def get_objects_from_directories(rootfileobject, key, prefix='', objects=[]):
    """
    Go through ROOT file and get objects from directories.
    Start with the directory 'key'.
    Append all objects found to ojects.
    Keep track of the total path with prefix.
    """
    if key == None: #.IsZombie():
        # skip key
        return objects
    if not key.IsFolder(): 
        objects.append(key)
        return objects
    else:
        keyname = key.GetTitle()
        #print 'keyname:', keyname
        dirname = os.path.join(prefix, keyname)
        directory = rootfileobject.GetDirectory(dirname)
        #print 'dirname:', dirname
        listofkeys = directory.GetListOfKeys()
        for key in listofkeys:
            #print 'key in list of keys', key
            prefix = dirname #os.path.join(dirname, key.GetTitle())
            #print 'prefix', prefix
            objects = get_objects_from_directories(rootfileobject, key, prefix=prefix, objects=objects)
        return objects





if __name__ == "__main__":
#def read_from_root_files(filenames):
    scriptdir = os.path.abspath(os.path.dirname(__file__))
    curdir = os.getcwd()
    os.chdir(scriptdir)
    ncalibsback = 1
    voltagethreshold = 0.7
    qties = []
    possible_quantities = {'va': 'Va', 'vana': 'Vana', 'vd': 'Vd', 'vbg': 'Vbg'}
    args = sys.argv
    for qty in possible_quantities:
        if qty in args:
            qties.append(qty)
            args.remove(qty)
    if qties == []:
        qties = possible_quantities # just do all
    givenfilenames = args[1:] #Run on the First given filename
    if givenfilenames != []: qties = ['fromfiles']


    print 'looking at', qties
    for qty in qties:
        if givenfilenames == []:
            #allfiles = sorted(glob.glob('/pixelscratch/pixelscratch/data1/BPix/Run_*/Run_*/' + possible_quantities[qty] + '*root'))
            allfiles =sorted(glob.glob('/afs/cern.ch/work/r/rhabibul/Runs/Run_*/' + possible_quantities[qty] + '*root'))

            filenames = allfiles[:] 
        else:
            filenames = givenfilenames
        for filepath in filenames:
            print filepath


\            mods = {}
            if '_' in filepath:
                qty = filepath[filepath.rindex('/') + 1:filepath.rindex('_')].lower()
            else:
                print "Skipping", filepath
                continue
            if qty not in ['va', 'vana', 'vd', 'vbg', 'vall']:
                print "Unknown readback quantity. Skipping", qty, filepath
                continue
            print 'Looking at ', qty

            table = qty
            t = rt.TFile(filepath)
            #maindir = "BPix"
            maindir = "BPix"
            d = t.GetDirectory(maindir)
            listofkeys = get_objects_from_directories(t, d)
            runnumber = int(filepath[filepath.rindex('Run_') + 4:filepath.rindex('/')])
            print ''
            print 'run number', runnumber
            time = datetime.datetime.isoformat(datetime.datetime.utcfromtimestamp(os.path.getmtime(filepath)))
            time = time.replace('T', ' ')
            print 'time:', time
            mods = {}
            rocs = {}
            print 'quantity:', table
            for key in listofkeys:

                    if key == None: continue # not present
                    roc = key.GetTitle()

                    # don't store adc
                    if 'ADC' in key.GetName():
                        meas = 'adc'
                    else:
                        meas = 'volt'

                    mod = roc[:roc.index('_ROC')]
                    if not mod in mods:
                        d = {'mod': mod, 'run': runnumber, 'date': time, 'rocvals': {}}
                        mods[mod] = d
                    else:
                        d = mods[mod]

                    histo = key.ReadObj()
                    entries = histo.GetEntries()
                    if entries == 0: continue # Skip zero entries!
                    droc = {'mod': mod, 'run': runnumber, 'date': time, 'roc': roc, 'qty': qty}

                    mean = histo.GetMean()
                    if mean == 0 and roc in mods[mod]['rocvals']:
                        # in addition to a nonzero value a zero value was read out
                        # ignore
                        continue
                    #elif mean != 0 roc in mods[mod]['rocvals'] and mods[mod]['rocvals'][roc] == 0:
                    # otherwise, just overwrite the value
                    mods[mod]['rocvals'][roc] = mean
                    if roc in rocs:
                        if meas in rocs[roc] and not (rocs[roc][meas] - 0.01 < mean < rocs[roc][meas] + 0.01):
                            print 'double measurement?'
                            print roc, rocs[roc]
                            print meas, mean
                        else:
                            rocs[roc][meas] = mean
                    else:
                        rocs[roc] = droc
                        rocs[roc][meas] = mean
#return rocs[roc]
            for roc in rocs.keys():
                ################# here is the dictionary ##########################
                print rocs[roc]
                        #return rocs
                #return rocs[roc]
                
                ###################################################################

           # for mod in mods.keys():
               #d = mods[mod]
               #rocvals = d.pop('rocvals')
               # allvalues = [rocvals[roc] for roc in rocvals]
               # if allvalues == []: continue # there were no entries! see GetEntries above
               # mean = sum(allvalues)/(1.0*len(allvalues))
               #std = math.sqrt(sum([(mean - num)**2 for num in allvalues])/(1.0*len(allvalues)))
               #print 'mean:', mean
               #print 'std:', std
               #d[qty + '_mean'] = mean
               #d[qty + '_std'] = std
               #mods[mod]['mean'] = mean
               #mods[mod]['std'] = std
               #mods[mod]['qty'] = qty
#allfiles =sorted(glob.glob('/afs/cern.ch/work/r/rhabibul/Runs/Run_*/' + possible_quantities[qty] + '*root'))  
#Master=read_from_root_files(filenames,allfiles)
#print Master
#Result=read_from_root_files(filenames)
#print Result
            os.chdir(curdir)
            






