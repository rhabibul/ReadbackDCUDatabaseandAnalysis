import pixdb
import cx_Oracle
from datetime import datetime
import glob 
import re

                                         #FUnction to read data from the Files 
def read_from_dcu_file(fnames):
    
    Parent=[]#initialize a list
    #should not be initialized in 

    for fname in fnames:
        with open(fname, 'r') as f:
            fstring = f.readlines()
            
        #current_sector = ''
        date = ''
        rog = ''
        for line in fstring:
            measurement = line.split()#splitting in white spaces and getting a list of words/strings
            measurement = [x.replace(",","") for x in measurement]#removes the comma's in the values by looping over the values in the list
           # print measurement
            if len(measurement)== 6:#Using length of measurement to get date 
                date=measurement[0]+"".join( measurement[1:3])+"_"+"".join(measurement[3:6])#formation of the date string
                #date ="".join(measurement[:3])+"_"+"".join(measurement[3:6]
                date=datetime.strptime(date,'%a%b%d_%H:%M:%S%Z%Y')
               
                continue
            if len(measurement)== 13 and 'ROG' in measurement[2]:
                rog=measurement[0]+"_"+measurement[1]+"_"+measurement[2]
                
                continue
            dictionary={}#dictionary to store a single line
            
            if len(measurement)==11 and ('bottom' in measurement[0] or 'top' in measurement[0]):
                dictionary.update({'tstamp':date,'rog':rog,'bottop':measurement[0]+"_"+measurement[1],'prt':measurement[3],'anaone':measurement[4],'digone':measurement[5],'anatwo':measurement[6],'digtwo':measurement[7],'rtd':measurement[8],'vbg':measurement[9],'intrtd':measurement[10]})
            
            else:
                continue
            #print dictionary
            Parent.append(dictionary)#adding the lists to he dictionary
    return Parent
            

fnames = ["dcu_read_20180603015822.txt","dcu_read_20180605145833.txt"]
Parent =read_from_dcu_file(fnames)
print Parent
        
                                               #writing data to a database
con = cx_Oracle.connect("occupancyproblems_admin/pixDB2018@devdb12u") #creates connection to the database
cur = con.cursor()#Does the necessary database operations
##cur.bindarraysize =len(Parent)
##cur.setinputsizes(20, int, 100)
sql_insert_line="INSERT INTO DCUSNAPSHOTS (tstamp,rog,bottop,prt,anaone,digione,anatwo,digitwo,rtd,vbg,intrtd) VALUES (:tstamp,:rog,:bottop,:prt,:anaone,:digone,:anatwo,:digtwo,:rtd,:vbg,:intrtd)"
##sql_insert_line=" INSERT INTO DCUSNAPSHOTS (date,rog,bottop,prt,anaone,digione,anatwo,digitwo,rtd,vbg,intrtd) VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11)"
#print Parent[0]
cur.prepare(sql_insert_line)#This is a better practice by programming protocol.
cur.executemany(None,Parent)
con.commit()
