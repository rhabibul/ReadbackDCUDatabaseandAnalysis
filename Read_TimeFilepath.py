import datetime
import glob
import re
import os
from collections import OrderedDict
#import pixdb
#Function to retrieve timestamp using the date of modification                                                                                                                                                                                                                 
def timestamp(fpath):
     
#print filepath                                                                                                                                                                                                                                                      
          time = datetime.datetime.isoformat(datetime.datetime.utcfromtimestamp(os.path.getmtime(fpath))) #getmtime():Get the time of last modification of the file     
          time = time.replace('T', ' ')
          # print time
          return time

                                                                                                                                                                                                                                                      

                                                                                                                                                                                                                                                                  

#Form a dictionary using all the values from the dat file
def Read_TimeFilepath(fnames):

          
          Parent={}
          for fname in fnames:
                    with open(fname, 'r') as f:
                              fstring = f.readlines()
                              path=f.name
                              #print path
                              date=timestamp(path)
                              #print date
                              Parent.setdefault(date,{}).update({'Path':path})
                    
          return Parent
fnames = glob.glob("/pixelscratch/pixelscratch/data0/Run_*/Run_*/Readbacker.dat")
#fnames =glob.glob("/pixelscratch/pixelscratch/data0/Run_319000/Run_319112/Readbacker.dat")
Final =Read_TimeFilepath(fnames)
#for i in Final:
          #print Final[i][0] +"->"+Final[i][1]
for key,vals in Final.iteritems():
          print key + '->' + vals['Path']



