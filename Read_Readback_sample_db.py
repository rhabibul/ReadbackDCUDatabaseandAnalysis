import datetime
import glob
import re
import os

#Function to retrieve timestamp using the date of modification                                                                                                                                                                                                                \
                                                                                                                                                                                                                                                                               
def timestamp(fpath):

#print filepath                                                                                                                                                                                                                                                                
          time = datetime.datetime.isoformat(datetime.datetime.utcfromtimestamp(os.path.getmtime(fpath))) #getmtime():Get the time of last modification of the file                                                                                                            
          time = time.replace('T', ' ')
          # print time                                                                                                                                                                                                                                                         
          return time





#Form a dictionary using all the values from the dat file                                                                                                                                                                                                                      
def Read_Readback_sample(fnames):


          Parent=[]

          #for filepath in fnames:                                                                                                                                                                                                                                             
          #print filepath                                                                                                                                                                                                                                                      
          #time = datetime.datetime.isoformat(datetime.datetime.utcfromtimestamp(os.path.getmtime(filepath)))                                                                                                                                                                  
          #time = time.replace('T', ' ')                                                                                                                                                                                                                                       
          #print time                                                                                                                                                                                                                                                          
          for fname in fnames:
                    with open(fname, 'r') as f:
                     fstring = f.readlines()
                     filepath=f.name
                     print filepath
                     date=timestamp(filepath)
                     print date
                     #print date                                                                                                                                                                                                                                               
                     name=''
                     dictoname={}
                    for line in fstring:
                              measurement = line.split()
                              if len(measurement)== 3 and ('FPix_' in measurement[1] or 'BPix_' in measurement[1]):
                                  name=measurement[1]
                                  dictoname.setdefault(measurement[2],{}).update({'Name':name,'Timestamp':date})#initialize a dictionary with the increasing integer as the key and the name and TImestamp as values added later                                         
                              if len(measurement)== 6 and not "#" in measurement[0]:
                                  dictoname.setdefault(measurement[0],{}).update({'vd':measurement[1],'va':measurement[2],'vana':measurement[3],'vbg':measurement[4],'iana':measurement[5]} )
          

                              else:
                                  continue
                    Parent.append(dictoname)
          return Parent


fnames = glob.glob("/pixelscratch/pixelscratch/data0/Run_*/Run_*/Readbacker.dat")

Final =Read_Readback_sample(fnames)
print Final



