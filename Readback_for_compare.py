import cx_Oracle
import datetime
import glob
import re
import os

#Function to retrieve timestamp using the date of modification                                                                                                                                                                                                                 
def timestamp(fnames):
     for filepath in fnames:
          #print filepath                                                                                                                                                                                                                                                      
          time = datetime.datetime.isoformat(datetime.datetime.utcfromtimestamp(os.path.getmtime(filepath)))#getmtime():Get the time of last modification of the file                                                                                                          
          time = time.replace('T', ' ')
          #print time                                                                                                                                                                                                                                                          
          return time

def Read_Readback_sample(fnames):
     for fname in fnames:
        with open(fname, 'r') as f:
             fstring = f.readlines()
             date=timestamp(fnames)
             #print date                                                                                                                                                                                                                                                       
        # name=''                                                                                                                                                                                                                                                              
        dictoname={}

        for line in fstring:
             measurement = line.split()
             if len(measurement)== 3 and ('FPix_' in measurement[1] or 'BPix_' in measurement[1]):
                  name=measurement[1]
                  dictoname.setdefault(measurement[2],{}).update({'Name':name,'Timestamp':date})#initialize a dictionary with the increasing integer as the key and the name and TImestamp as values added alter                                                               
             if len(measurement)== 6 and not "#" in measurement[0]:
                  dictoname.setdefault(measurement[0],{}).update({'vd':measurement[1],'va':measurement[2],'vana':measurement[3],'vbg':measurement[4],'iana':measurement[5]} )#again initialize the dictionary with again the increasing integer as the key and all the quantiies as values.                                                                                                                                                                                                                                                                 


             else:
                  continue
        print dictoname








if __name__=="__main__"

fnames = glob.glob("/afs/cern.ch/work/r/rhabibul/ReadbackFiles/")

Parent =Read_Readback_sample(fnames)
                                                                                                                                                                                                                                                                 




