import argparse                                                                                                                                                                                                                                    
import datetime
import glob
import re
import os
import pixdb                                                                                                                                                                                                                                                                  
#Function to retrieve timestamp using the date of modification                                                                                                                                                                                                                \
                                                                                                                                                                                                                                                                               
def timestamp(fpath):

#print filepath                                                                                                                                                                                                                                                                
          time = datetime.datetime.isoformat(datetime.datetime.utcfromtimestamp(os.path.getmtime(fpath))) #getmtime():Get the time of last modification of the file                                                                                                            
          time = time.replace('T', ' ')
          # print time                                                                                                                                                                                                                                                         
          return time





#Form a dictionary using all the values from the dat file                                                                                                                                                                                                                      
def Read_Readback_sample(fname):


    #Parent=[]
    
    with open(fname, 'r') as f:
        fstring = f.readlines()
        filepath=f.name
                     #print filepath                                                                                                                                                                                                                                           
        date=timestamp(filepath)
                     #print date                                                                                                                                                                                                                                               
                     #print date                                                                                                                                                                                                                                               
        name=''
        dictoname={}
        for line in fstring:
            measurement = line.split()
            if len(measurement)== 3 and ('FPix_' in measurement[1] or 'BPix_' in measurement[1]):
                name=measurement[1]
                dictoname.setdefault(measurement[2],{}).update({'Name':name,'Timestamp':date})#initialize a dictionary with the increasing integer as the key and the name and TImestamp as values added later                                         
            if len(measurement)== 6 and not "#" in measurement[0]:
                dictoname.setdefault(measurement[0],{}).update({'vd':measurement[1],'va':measurement[2],'vana':measurement[3],'vbg':measurement[4],'iana':measurement[5]} )
                    
            else:
                continue
            
    return dictoname
                
#fnames = glob.glob("/pixelscratch/pixelscratch/data0/Run_*/Run_*/Readbacker.dat")
#fnames =glob.glob("/pixelscratch/pixelscratch/data0/Run_319000/Run_319112/Readbacker.dat")                                                                                                                                                                                   






def write_to_db(Final):

    with pixdb.PixdbWriteInterface() as interface:
    
        sql_query_part_ids = "SELECT cms_pix_names.name,cms_pix_parts.part_id from cms_pix_names JOIN cms_pix_part_name ON cms_pix_names.name_id=cms_pix_part_name.name_id JOIN cms_pix_parts ON cms_pix_parts.part_id=cms_pix_part_name.part_id"
        
        sql_query_catalog_id = "SELECT cms_pix_measurement_types.measurement_type ,cms_pix_measurement_catalog.measurement_catalog_id FROM cms_pix_measurement_catalog JOIN cms_pix_measurement_types ON cms_pix_measurement_types.measurement_type_id = cms_pix_measurement_catalog.measurement_type_id JOIN cms_pix_part_types ON cms_pix_part_types.part_type_id=cms_pix_measurement_catalog.part_type_id WHERE part_type LIKE 'ROC'"
        part_ids=dict(interface.query(sql_query_part_ids))
        
        measurement_catalog_ids=dict(interface.query(sql_query_catalog_id))
       # print measurement_catalog_ids
    #for line in readback_dicts:
        for ikey,vals in Final.iteritems():
            date= vals['Timestamp']
            iov_date=datetime.datetime.strptime(date,'%Y-%m-%d %H:%M:%S')
            #print iov_date                                                                                                                                                                                                                                                    
            #print iov_date                                                                                                                                                                                                                                                    
            part_id=part_ids[vals['Name']]
            #print part_id                                                                                                                                                                                                                                                     
            for qty, val in vals.iteritems():
                if not qty in measurement_catalog_ids:
                    continue
                measurement_catalog_id=measurement_catalog_ids[qty]
            #measurement = [(val,None)]
                interface.insert_measurement(part_id,measurement_catalog_id,iov_date,float(val))
            
if __name__=="__main__":


    #fname= '/afs/cern.ch/work/r/rhabibul/ReadbackFiles/Readbacker_317852.dat' # this shall become an arg
    ArgParser=argparse.ArgumentParser(description='Writing Readbacker file to pixDB') #gives you a description
    ArgParser.add_argument('-p','--path',dest='fname',action='store',help='Path to Readbacker.dat file') #tells you what flag does what...and to store the arguement
    args= ArgParser.parse_args() #this tells you to parse the arguement given
    #print args.fname
    Final =Read_Readback_sample(args.fname)
   #print Final
    write_to_db(Final)
    
