import ROOT as rt
import os
import datetime
import glob
import math
import sys
import pixdb



def get_objects_from_directories(rootfileobject, key, prefix='', objects=[]):
    """                                                                                                                                                                                                                                                                        
    Go through ROOT file and get objects from directories.                                                                                                                                                                                                                     
    Start with the directory 'key'.                                                                                                                                                                                                                                            
    Append all objects found to ojects.                                                                                                                                                                                                                                        
    Keep track of the total path with prefix.                                                                                                                                                                                                                                  
    """
    if key == None: #.IsZombie():                                                                                                                                                                                                                                              
        # skip key                                                                                                                                                                                                                                                             
        return objects
    if not key.IsFolder():
        objects.append(key)
        return objects
    else:
        keyname = key.GetTitle()
        #print 'keyname:', keyname                                                                                                                                                                                                                                             
        dirname = os.path.join(prefix, keyname)
        directory = rootfileobject.GetDirectory(dirname)
        #print 'dirname:', dirname                                                                                                                                                                                                                                             
        listofkeys = directory.GetListOfKeys()
        for key in listofkeys:
            #print 'key in list of keys', key                                                                                                                                                                                                                                  
            prefix = dirname #os.path.join(dirname, key.GetTitle())                                                                                                                                                                                                            
            #print 'prefix', prefix                                                                                                                                                                                                                                            
            objects = get_objects_from_directories(rootfileobject, key, prefix=prefix, objects=objects)
    return objects







def gettimestamp_from_run(filepath):
    time = datetime.datetime.isoformat(datetime.datetime.utcfromtimestamp(os.path.getmtime(filepath)))
    time = time.replace('T', ' ')
    return time




def readback_dict_from_rootfile(filename):
    """
    take root file and return dictionary.
    """
    #for filepath in filenames:
    with open(filename, 'r') as f:
        filepath=f.name
        
    if '_' in filepath:
        qty = filepath[filepath.rindex('/') + 1:filepath.rindex('_')].lower()
    else:
        print "Skipping", filepath
        #continue
    if qty not in ['va', 'vana', 'vd', 'vbg', 'vall']:
        print "Unknown readback quantity. Skipping", qty, filepath
        #continue
    print 'Looking at ', qty
    
    table = qty
    t = rt.TFile(filepath)
    maindir = "BPix"
    d = t.GetDirectory(maindir)
    listofkeys = get_objects_from_directories(t, d)
    runnumber = int(filepath[filepath.rindex('Run_') + 4:filepath.rindex('/')])
    print ''
    print 'run number', runnumber
       #time = datetime.datetime.isoformat(datetime.datetime.utcfromtimestamp(os.path.getmtime(filepath)))
       #time = time.replace('T', ' ')
    time=gettimestamp_from_run(filepath)
    print 'time:', time
        
    mods = {}
    rocs = {}
    print 'quantity:', table
    for key in listofkeys:
        if key == None: continue # not present                                                                                                                                                                                                                     
        roc = key.GetTitle()
        
            # don't store adc                                                                                                                                                                                                                                          
        if 'ADC' in key.GetName():
            meas = 'adc'
        else:
            meas = 'volt'
            
            mod = roc[:roc.index('_ROC')]
            if not mod in mods:
                d = {'mod': mod, 'run': runnumber, 'date': time, 'rocvals': {}}
                mods[mod] = d
            else:
                d = mods[mod]
                
                histo = key.ReadObj()
                entries = histo.GetEntries()
                if entries == 0: continue # Skip zero entries!                                                                                                                                                                                                             
                droc = {'mod': mod, 'run': runnumber, 'date': time, 'roc': roc, 'qty': qty}
               # print mods[mod]
                mean = histo.GetMean()
                if mean == 0 and roc in mods[mod]['rocvals']:
                    # in addition to a nonzero value a zero value was read out                                                                                                                                                                                             
                    # ignore                                                                                                                                                                                                                                               
                    continue
                
                mods[mod]['rocvals'][roc] = mean
                if roc in rocs:
                    if meas in rocs[roc] and not (rocs[roc][meas] - 0.01 < mean < rocs[roc][meas] + 0.01):
                        print 'double measurement?'
                        print roc, rocs[roc]
                        print meas, mean
                    else:
                        rocs[roc][meas] = mean
                else:
                    rocs[roc] = droc
                    rocs[roc][meas] = mean
                    #dicto={}
                    #dicto2={}
    #return roc
    #print rocs
    #print type(rocs)
    #print rocs
    return rocs
    #print type(rocs)
    #for roc in rocs.keys():
        #print rocs
        #return rocs[roc]
    #print type(rocs[roc])    
        
        
if __name__== "__main__":
    possible_quantities = {'va': 'Va', 'vana': 'Vana', 'vd': 'Vd', 'vbg': 'Vbg'}
    rog=[]
    for qty in possible_quantities:
        #allfiles = sorted(glob.glob('/pixelscratch/pixelscratch/data1/BPix/Run_*/Run_*/' + possible_quantities[qty] + '*root'))
        allfiles =sorted(glob.glob('/afs/cern.ch/work/r/rhabibul/Runs/Run_*/' + possible_quantities[qty] + '*root'))
        filenames=allfiles
        #print filenames
        for filename in filenames:
            new_rocs_dict = readback_dict_from_rootfile(filename)
            #print new_rocs_dict
            rog.append(new_rocs_dict)
    print rog
